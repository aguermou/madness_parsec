extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 *
 * @precisions normal z -> s d c
 *
 *
 * This jdf returns the value of the one norm of a matrix A
 * where the one norm  of a matrix is the maximum column sum.
 *
 * This jdf is optimized for 2D-Block cyclic distributed data with a grid
 * P-by-Q.
 * The first step sums the local data of each columns.
 * The second finishes the sums on each columns. At the end of this step, all P
 * processes belonging to a column have the same data.
 * The third step search the local maximum.
 * The fourth step combines the local maxima together. At the end all processes
 * owns the same value.
 *
 * The reductions are down by a pipeline folowed by a broadcast of the results.
 *
 */
#include <math.h>
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

/* Globals
 */
P            [type = "int"]
Q            [type = "int"]
ntype        [type = "PLASMA_enum"]
uplo         [type = "PLASMA_enum"]
diag         [type = "PLASMA_enum"]
dataA        [type = "dague_ddesc_t *"]
Tdist        [type = "dague_ddesc_t *"]
norm         [type = "double *"]

descA        [type = "tiled_matrix_desc_t" hidden=on default="*((tiled_matrix_desc_t*)dataA)" ]
minMNT       [type = "int" hidden=on default="dplasma_imin( descA.mt, descA.nt )" ]
minMN        [type = "int" hidden=on default="dplasma_imin( descA.m,  descA.n )"  ]
MT           [type = "int" hidden=on default="(uplo == PlasmaUpper) ? minMNT : descA.mt"]
NT           [type = "int" hidden=on default="(uplo == PlasmaLower) ? minMNT : descA.nt"]
M            [type = "int" hidden=on default="(uplo == PlasmaUpper) ? minMN  : descA.m"]
N            [type = "int" hidden=on default="(uplo == PlasmaLower) ? minMN  : descA.n"]

RWCOL(m, n) [profile = off]

    // Execution space
    m = 0 .. P-1
    n = 0 .. NT-1
    row = inline_c %{
    switch (uplo) {
    case PlasmaLower:
    {
        int tmpm = MT-((MT-m-1)%P)-1;
        if (( m > MT-1 ) || ( tmpm < n ))
            return MT;
        else
            return tmpm;
    }
    break;
    case PlasmaUpper:
    {
        if ( m > n )
            return MT;
        else {
            int nmax = dplasma_imin( n, MT-1 );
            return nmax-((nmax-m)%P);
        }
    }
    break;
    default:
      if ( m > MT-1 )
          return MT;
      else
          return MT-((MT-m-1)%P)-1;
    }
    %}

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    WRITE W -> (m <  MT   ) ? W STEP1(row, n)  [type = COL]
            -> (m > (MT-1)) ? W STEP2(m,   n)  [type = COL]

BODY
{
#if !defined(DAGUE_DRY_RUN)
    /* Initialize W */
    memset( W, 0, descA.nb * sizeof(double) );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

RWELT(m, n) [profile = off]

    // Execution space
    m = 0 .. P-1
    n = 0 .. Q-1
    col = inline_c %{
      return NT-((NT-n-1)%Q)-1;
    %}

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    WRITE W -> (n <  NT   ) ? W STEP3(m, col)  [type = ELT]
            -> (n > (NT-1)) ? W STEP4(m,   n)  [type = ELT]

BODY
{
#if !defined(DAGUE_DRY_RUN)
    /* Initialize W */
    ((double *)W)[0] = 0.;
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *
 *                    STEP 1
 *
 *  For j in [1,Q], W(m, j) = reduce( A(m, j+k*Q) )
 *
 **************************************************/
STEP1(m,n)

    // Execution space
    m = 0 .. MT-1
    nmin = inline_c %{ if (uplo == PlasmaUpper ) return m; else return 0; %}
    nmax = inline_c %{ if (uplo == PlasmaLower ) return dplasma_imin(m, NT-1); else return NT-1; %}
    n = nmin .. nmax

    mmin = inline_c %{ if (uplo == PlasmaLower ) return n; else return 0; %}
    mmax = inline_c %{ if (uplo == PlasmaUpper ) return dplasma_imin(n, MT-1); else return MT-1; %}

    // Parallel partitioning
    :dataA(m, n)

    // Parameters
    READ A <-  dataA(m, n)
    RW   W <-  ( m < (mmax+1-P)) ? W STEP1( m+P, n ) : W RWCOL( m%P, n ) [type = COL]
           ->  ( m < (mmin+P)  ) ? W STEP2( m%P, n ) : W STEP1( m-P, n ) [type = COL]

BODY
{
    int tempmm = ( m == (MT-1) ) ? M - m * descA.mb : descA.mb;
    int tempnn = ( n == (NT-1) ) ? N - n * descA.nb : descA.nb;
    int ldam = BLKLDD( descA, m );

    printlog("thread %d zlange STEP1(%d, %d)\n"
             "\t( tempmm=%d, tempnn=%d, A(%d, %d)[%p], lda=%d, W(%d,%d)[%p])\n",
             context->th_id, m, n, tempmm, tempnn, m, n, A, ldam, m, n%Q, W);

#if !defined(DAGUE_DRY_RUN)
    if ( (m == n) && (uplo != PlasmaUpperLower) ) {
        CORE_ztrasm( PlasmaColumnwise, uplo, diag,
                     tempmm, tempnn,
                     A, ldam, W);
    }
    else {
        CORE_dzasum(PlasmaColumnwise, PlasmaUpperLower,
                    tempmm, tempnn,
                    A, ldam, W);
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *                    STEP 2
 *
 *  For each j, W(m, j) = reduce( W(m, 0..Q-1) )
 *
 **************************************************/
STEP2(m, n)

    // Execution space
    m = 0 .. P-1
    n = 0 .. NT-1
    mmin = inline_c %{ if (uplo == PlasmaLower ) return n; else return 0; %}
    row = inline_c %{
      return mmin + m - mmin%P;
    %}

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ A <- ( m   ==  0     ) ?   Tdist(m, n)      : W STEP2(m-1, n) [type = COL]
    RW   W <- ( row >  (MT-1) ) ? W RWCOL(m, n)      : W STEP1(row, n) [type = COL]
           -> ( m   == (P-1)  ) ? A STEP3(0..P-1, n) : A STEP2(m+1, n) [type = COL]

BODY
{
    int tempnn = ( n == (NT-1) ) ? N - n * descA.nb : descA.nb;

    printlog("thread %d zlange STEP2(%d, %d)\n"
             "\t( tempnn=%d, W(%d, %d) + W(%d, %d)\n",
             context->th_id, m, n, tempnn, m-1, n, m, n);

#if !defined(DAGUE_DRY_RUN)
    if(m > 0)
    {
        cblas_daxpy( tempnn, 1., A, 1, W, 1);
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END


/**************************************************
 *
 *                    STEP3
 *
 * For m in 0..P-1, W(m, n) = max( W(m..mt[P], n ) )
 *
 **************************************************/
STEP3(m, n)

    // Execution space
    m = 0 .. P-1
    n = 0 .. NT-1

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ A <- W STEP2(P-1, n)                                      [type = COL]
    RW   W <- (n < (NT-Q)) ? W STEP3( m, n+Q ) : W RWELT( m, n%Q ) [type = ELT]
           -> (n < Q     ) ? W STEP4( m, n   ) : W STEP3( m, n-Q ) [type = ELT]

BODY
{
    int tempnn = ( n == (NT-1) ) ? N - n * descA.nb : descA.nb;

    printlog("thread %d zlange STEP3(%d, %d)\n",
             context->th_id, m, n);

#if !defined(DAGUE_DRY_RUN)
    double *dA = (double*)A;
    double *dW = (double*)W;
    double maxval = 0;
    int i;

    for(i = 0; i < tempnn; i++, dA++)
        maxval = ( maxval > *dA ) ? maxval : *dA;

    if ( n < (NT-Q) ) {
        *dW = ( maxval > *dW ) ? maxval : *dW;
    } else {
        *dW = maxval;
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *                    STEP 4
 *
 *  For each i, W(i, n) = max( W(0..P-1, n) )
 *
 **************************************************/
STEP4(m,n)

    // Execution space
    m = 0..P-1
    n = 0..Q-1

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ A <- ( n ==  0     ) ?   Tdist(m, n)          : W STEP4(m, n-1) [type = ELT]
    RW   W <- ( n >  (NT-1) ) ? W RWELT(m, n)          : W STEP3(m, n  ) [type = ELT]
           -> ( n == (Q-1)  ) ? W WRITE_RES(m, 0..Q-1) : A STEP4(m, n+1) [type = ELT]

BODY
{
    printlog("thread %d zlange STEP4(%d, %d)\n",
             context->th_id, m, n);

#if !defined(DAGUE_DRY_RUN)
        double *dA = (double*)A;
        double *dW = (double*)W;
        if(n > 0)
            *dW = ( *dA > *dW ) ? *dA : *dW;
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *                      STEP5                     *
 **************************************************/
WRITE_RES(m,n)

    // Execution space
    m = 0..P-1
    n = 0..Q-1

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ W <- W STEP4( m, Q-1 )   [type = ELT]

BODY
{
    *norm = *( (double*)W );
}
END
