extern "C" %{
/*
 *  Copyright (c) 2012
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> s d c
 *
 */
#define PRECISION_z

#include "dague.h"
#include <math.h>
#include <core_blas.h>
#include <core_blas.h>

#include "data_distribution.h"
#include "data_dist/matrix/precision.h"
#include "data_dist/matrix/matrix.h"
#include "dplasma/lib/memory_pool.h"
#include "dplasma/lib/dplasmajdf.h"

//#define PRIO_YVES1

#if defined(PRIO_YVES1)
#define GETPRIO_PANEL( __m, __n )      descA.mt * descA.nt - ((descA.nt - (__n) - 1) * descA.mt + (__m) + 1)
#define GETPRIO_UPDTE( __m, __n, __k ) descA.mt * descA.nt - ((descA.nt - (__n) - 1) * descA.mt + (__m) + 1)
#elif defined(PRIO_YVES2)
#define GETPRIO_PANEL( __m, __n )      descA.mt * descA.nt - ((__m) * descA.nt + descA.nt - (__n))
#define GETPRIO_UPDTE( __m, __n, __k ) descA.mt * descA.nt - ((__m) * descA.nt + descA.nt - (__n))
#elif defined(PRIO_MATHIEU1)
#define GETPRIO_PANEL( __m, __n )      (descA.mt + (__n) - (__m) - 1) * descA.nt + (__n)
#define GETPRIO_UPDTE( __m, __n, __k ) (descA.mt + (__n) - (__m) - 1) * descA.nt + (__n)
#elif defined(PRIO_MATHIEU2)
#define GETPRIO_PANEL( __m, __n )      ((dplasma_imax(descA.mt, descA.nt) - dplasma_imax( (__n) - (__m), (__m) - (__n) ) -1 ) * 12 + (__n))
#define GETPRIO_UPDTE( __m, __n, __k ) ((dplasma_imax(descA.mt, descA.nt) - dplasma_imax( (__n) - (__m), (__m) - (__n) ) -1 ) * 12 + (__n))
#elif defined(PRIO_MATYVES)
#define FORMULE( __x ) ( ( -1. + dplasma_dsqrt( 1. + 4.* (__x) * (__x)) ) * 0.5 )
#define GETPRIO_PANEL( __m, __k )      (int)( 22. * (__k) + 6. * ( FORMULE( descA.mt ) - FORMULE( (__m) - (__k) + 1. ) ) )
#define GETPRIO_UPDTE( __m, __n, __k ) (int)( (__m) < (__n) ? GETPRIO_PANEL( (__n), (__n) ) - 22. * ( (__m) - (__k) ) - 6. * ( (__n) - (__m) ) \
                                              :               GETPRIO_PANEL( (__m), (__n) ) - 22. * ( (__n) - (__k) ) )
#else
  /*#warning running without priority*/
#define GETPRIO_PANEL( __m, __n )      0
#define GETPRIO_UPDTE( __m, __n, __k ) 0
#endif

%}

A      [type = "dague_ddesc_t *"]
descA  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)A)"]
IPIV   [type = "dague_ddesc_t *" aligned=A]
LT     [type = "dague_ddesc_t *" aligned=A]
descLT [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)LT)"]
qrtree [type = "dplasma_qrtree_t"]
ib     [type = "int"]
p_work [type = "dague_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)*ib*(descLT.nb))"]
p_tau  [type = "dague_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)   *(descLT.nb))"]
INFO   [type = "int*"]

minMN  [type = "int" hidden=on default="( (descA.mt < descA.nt) ? descA.mt : descA.nt )" ]

/**
 * GETRF kernel
 *
 * There are dplasma_qr_getnbgeqrf( pivfct, k, descA.mt ) getrf applyed at step
 * k on the rows indexed by m.
 * nextm is the first row that will be killed by the row m at step k.
 * nextm = descA.mt if the row m is never used as a killer.
 *
 */
zgetrf(k, i)
  /* Execution space */
  k = 0 .. minMN-1
  i = 0 .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  m      = inline_c %{ return qrtree.getm(       &qrtree, k, i); %}
  nextm  = inline_c %{ return qrtree.nextpiv(    &qrtree, k, m, descA.mt); %}

  SIMCOST 4

  /* Locality */
  : A(m, k)

  RW    A <- ( k == 0 ) ? A(m, k)
          <- ( k >  0 ) ? A2 zttmqr(k-1, m, k )

          -> A zgetrf_typechange(k, i)

          -> ( k == descA.mt-1 ) ? A(m, k)                                     [type = UPPER_TILE]
          -> ( (k < descA.mt-1) & (nextm != descA.mt) ) ?  A1 zttqrt(k, nextm) [type = UPPER_TILE]
          -> ( (k < descA.mt-1) & (nextm == descA.mt) ) ?  A2 zttqrt(k, m)     [type = UPPER_TILE]
  RW    P <- IPIV(m, k)                                                        [type = PIVOT]
          -> IPIV(m, k)                                                        [type = PIVOT]
          -> (k < descA.nt-1) ? P zgessm(k, i, (k+1)..(descA.nt-1))            [type = PIVOT]

  ; inline_c %{ return GETPRIO_PANEL(m, k); %}

BODY
{
    int iinfo = 0;
    int tempmm = (m == (descA.mt-1)) ? (descA.m - m * descA.mb) : descA.mb;
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldam   = BLKLDD( descA, m );

    printlog("zgetrf_incpiv( k=%d, i=%d, m=%d, nextm=%d)\n"
             "\t( tempmm=%d, tempkn=%d, ib, A(%d,%d)[%p], ldam=%d, IPIV(%d,%d)[%p]) => info = %d\n",
             k, i, m, nextm, tempmm, tempkn, m, k, A, ldam, m, k, P,
             (iinfo != 0 ? m * descA.mb + iinfo : 0 ));

#if !defined(DAGUE_DRY_RUN)
           /* Set local IPIV to 0 before generation
            * Better here than a global initialization for locality
            * and it's also done in parallel */
           memset(P, 0, dplasma_imin(tempkn, tempmm) * sizeof(int) );

           CORE_zgetrf_incpiv(tempmm, tempkn, ib,
                              A /* A(m, k)    */, ldam,
                              P /* IPIV(m, k) */, &iinfo );

           if ( (iinfo != 0) /*&& (k == descA.mt-1)*/ ) {
               *INFO = k * descA.mb + iinfo; /* Should return if enter here */
               fprintf(stderr, "\nWarning!!! zgetrf( k=%d, i=%d, m=%d) failed => %d\n", k, i, m, *INFO );
           }
#endif /* !defined(DAGUE_DRY_RUN) */

#if defined(DAGUE_SIM)
    if ( descA.mt-1 == k )
        ((PLASMA_Complex64_t*)A)[0] = (PLASMA_Complex64_t)(this_task->sim_exec_date);
#endif
}
END

/**
 * zgetrf_typechange()
 *
 * Task to distinguish upper/lower part of the tile
 */
zgetrf_typechange(k, i) [profile = off]
  /* Execution space */
  k = 0 .. minMN-1
  i = 0 .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  m =      inline_c %{ return qrtree.getm(       &qrtree, k, i); %}

  : A(m, k)

  RW A <- A zgetrf(k, i)
       -> ( descA.nt-1 > k ) ? A zgessm(k, i, (k+1)..(descA.nt-1)) [type = LOWER_TILE]
       -> A(m, k)                                                  [type = LOWER_TILE]
BODY
{
    /* Nothing */
}
END

/**
 * zgessm()
 *
 * (see zgetrf() for details on definition space)
 */
zgessm(k, i, n)
  /* Execution space */
  k = 0   .. minMN-1
  i = 0   .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  n = k+1 .. descA.nt-1
  m     = inline_c %{ return qrtree.getm(    &qrtree, k, i); %}
  nextm = inline_c %{ return qrtree.nextpiv( &qrtree, k, m, descA.mt); %}

  SIMCOST 6

  /* Locality */
  : A(m, n)

  READ  A <- A zgetrf_typechange(k, i)                                   [type = LOWER_TILE]
  READ  P <- P zgetrf(k, i)                                              [type = PIVOT]

  RW    C <- ( 0 == k ) ? A(m, n)
          <- ( k >  0 ) ? A2 zttmqr(k-1, m, n)
          -> ( k == descA.mt-1 ) ? A(m, n)
          -> ( (k < descA.mt-1) & (nextm != descA.mt) ) ? A1 zttmqr(k, nextm, n)
          -> ( (k < descA.mt-1) & (nextm == descA.mt) ) ? A2 zttmqr(k, m,     n)

    ; inline_c %{ return GETPRIO_UPDTE(m, n, k); %}

BODY
{
    int tempmm = (m == (descA.mt-1)) ? (descA.m - m * descA.mb) : descA.mb;
    int tempnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int ldam   = BLKLDD( descA, m );

    printlog("zgessm( k=%d, i=%d, n=%d, m=%d, nextm=%d)\n"
              "\t( tempmm=%d, tempnn=%d, tempmm=%d, ib, IPIV(%d,%d)[%p], \n"
              "\t  A(%d,%d)[%p], ldam = %d, A(%d,%d)[%p], ldam = %d)\n",
              k, i, n, m, nextm, tempmm, tempnn, tempmm, m, k, P, m, k, A, ldam, m, n, C, ldam);

#if !defined(DAGUE_DRY_RUN)
           CORE_zgessm(tempmm, tempnn, tempmm, ib,
                       P /* IPIV(m,k) */,
                       A /* A(m,k)    */, ldam,
                       C /* A(m,n)    */, ldam );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END


/**
 * zttqrt()
 *
 * The row p kills the row m.
 * nextp is the row that will be killed by p at next stage of the reduction.
 * prevp is the row that has been killed by p at the previous stage of the reduction.
 * prevm is the row that has been killed by m at the previous stage of the reduction.
 * type defines the operation to perform: TS if 0, TT otherwise
 * ip is the index of the killer p in the sorted set of killers for the step k.
 * im is the index of the killer m in the sorted set of killers for the step k.
 *
 */
zttqrt(k, m)
  /* Execution space */
  k = 0   .. minMN-1
  m = k+1 .. descA.mt-1
  p =     inline_c %{ return qrtree.currpiv( &qrtree, k, m);    %}
  nextp = inline_c %{ return qrtree.nextpiv( &qrtree, k, p, m); %}
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, p, m); %}
  prevm = inline_c %{ return qrtree.prevpiv( &qrtree, k, m, m); %}
  type  = inline_c %{ return qrtree.gettype( &qrtree, k, m );   %}
  ip    = inline_c %{ return qrtree.geti(    &qrtree, k, p );   %}
  im    = inline_c %{ return qrtree.geti(    &qrtree, k, m );   %}

  SIMCOST inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? 6 : 2; %}

  : A(m, k)

  RW   A1 <- (   prevp == descA.mt ) ? A zgetrf(k, ip ) : A1 zttqrt(k, prevp ) [type = UPPER_TILE]
          -> (   nextp != descA.mt ) ? A1 zttqrt(k, nextp )                    [type = UPPER_TILE]
          -> ( ( nextp == descA.mt ) & (p == k) ) ? A zttqrt_out_A1(k)         [type = UPPER_TILE]
          -> ( ( nextp == descA.mt ) & (p != k) ) ? A2 zttqrt(k, p)            [type = UPPER_TILE]

  RW   A2 <- ( (type == 0) && (k     == 0        ) ) ? A(m, k)
          <- ( (type == 0) && (k     != 0        ) ) ? A2 zttmqr(k-1, m, k )
          <- ( (type != 0) && (prevm == descA.mt ) ) ? A zgetrf(k, im )        [type = UPPER_TILE]
          <- ( (type != 0) && (prevm != descA.mt ) ) ? A1 zttqrt(k, prevm )    [type = UPPER_TILE]

          -> (type == 0 ) ? A(m, k)
          -> (type != 0 ) ? A(m, k)                                             [type = UPPER_TILE]

          -> (type == 0) &&  (descA.nt-1 > k) ? V zttmqr(k, m, (k+1)..(descA.nt-1))
          -> (type != 0) &&  (descA.nt-1 > k) ? V zttmqr(k, m, (k+1)..(descA.nt-1)) [type = UPPER_TILE]

  RW   L  <- LT(m, k)                                                                     [type = SMALL_L]
          -> LT(m, k)                                                                     [type = SMALL_L]
          -> (descA.nt-1 > k)? L zttmqr(k, m, (k+1)..(descA.nt-1))                        [type = SMALL_L]

  RW   P  <- IPIV(m,k)                                                                    [type = PIVOT]
          ->  ( type == 0 ) ? IPIV(m,k)                                                   [type = PIVOT]
          -> (( type == 0 ) && (descA.nt-1 > k) ) ? P zttmqr(k, m, (k+1)..(descA.nt-1))   [type = PIVOT]

 ; inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? GETPRIO_PANEL(p, k) : GETPRIO_PANEL(m, k); %}

BODY
{
    int tempmm = ((m)==((descA.mt)-1)) ? ((descA.m)-(m*(descA.mb))) : (descA.mb);
    int tempkn = ((k)==((descA.nt)-1)) ? ((descA.n)-(k*(descA.nb))) : (descA.nb);
    int ldap = BLKLDD( descA, p );
    int ldam = BLKLDD( descA, m );

    printlog("zttqrt( k=%d, m=%d, p=%d, nextp=%d, prevp=%d, prevm=%d, type=%d, ip=%d, im=%d )\n"
             "\t(tempmm=%d, tempkn=%d, ib=%d, descLT.nb=%d,\n"
             "\t A(%d,%d)[%p], ldap=%d, A(%d,%d)[%p], ldam=%d,\n"
             "\t LT(%d,%d)[%p], descLT.mb=%d, descLT.nb=%d)\n",
             k, m, p, nextp, prevp, prevm, type, ip, im,
             tempmm, tempkn, ib, descLT.nb,
             p, k, A1, ldap, m, k, A2, ldam, m, k, L, descLT.mb, descLT.nb);

#if !defined(DAGUE_DRY_RUN)
        void *p_elem_B = dague_private_memory_pop( p_work );

        if ( type == DPLASMA_QR_KILLED_BY_TS ) {
            int iinfo = 0;
            memset(P, 0, dplasma_imin(tempkn, tempmm) * sizeof(int) );
            CORE_ztstrf(tempmm, tempkn, ib, descLT.nb,
                        A1 /* A(p,k)    */, ldap,
                        A2 /* A(m,k)    */, ldam,
                        L  /* L(m,k)    */, descLT.mb,
                        P  /* IPIV(m,k) */,
                        p_elem_B, descLT.nb, &iinfo );

            if ( (iinfo != 0) /*&& (m == descA.mt-1)*/ ) {
                *INFO = k * descA.mb + iinfo; /* Should return if enter here */
                fprintf(stderr, "ztstrf(%d, %d) failed => %d\n", m, k, *INFO );
            }
        } else {
            void *p_elem_A = dague_private_memory_pop( p_tau  );

            CORE_zttqrt(
                tempmm, tempkn, ib,
                A1 /* A(p, k)  */, descA.mb /*ldap*/,
                A2 /* A(m, k)  */, ldam,
                L  /* LT(m, k) */, descLT.mb,
                p_elem_A, p_elem_B );

            dague_private_memory_push( p_tau , p_elem_A );
        }

        dague_private_memory_push( p_work, p_elem_B );
#endif /* !defined(DAGUE_DRY_RUN) */

#if defined(DAGUE_SIM)
    ((PLASMA_Complex64_t*)A2)[0] = (PLASMA_Complex64_t)(this_task->sim_exec_date);
    if ( ( ( nextp == descA.mt ) & (p == k) ) )
      ((PLASMA_Complex64_t*)A1)[0] = (PLASMA_Complex64_t)(this_task->sim_exec_date);
#endif
}
END

zttqrt_out_A1(k) [profile = off]
  k = 0..( (descA.mt <= descA.nt) ? descA.mt-2 : descA.nt-1 )
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, k, k ); %}

  : A(k, k)

  RW A <- A1 zttqrt( k, prevp ) [type = UPPER_TILE]
       -> A(k, k)               [type = UPPER_TILE]
BODY
{
    /* nothing */
}
END

/**
 * zttmqr()
 *
 * See also zttqrt()
 * type1 defines the operations to perfom at next step k+1 on the row m
 *   if type1 == 0, it will be a TS so the tile goes to a TTQRT/TTMQR operation
 *   if type1 != 0, it will be a TT so the tile goes to a GEQRT/UNMQR operation
 * im1 is the index of the killer m at the next step k+1 if its type is !0, descA.mt otherwise
 *
 */
zttmqr(k, m, n)
  /* Execution space */
  k = 0   .. minMN-1
  m = k+1 .. descA.mt-1
  n = k+1 .. descA.nt-1
  p =     inline_c %{ return qrtree.currpiv( &qrtree, k,   m);    %}
  nextp = inline_c %{ return qrtree.nextpiv( &qrtree, k,   p, m); %}
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k,   p, m); %}
  prevm = inline_c %{ return qrtree.prevpiv( &qrtree, k,   m, m); %}
  type  = inline_c %{ return qrtree.gettype( &qrtree, k,   m );   %}
  type1 = inline_c %{ return qrtree.gettype( &qrtree, k+1, m );   %}
  ip    = inline_c %{ return qrtree.geti(    &qrtree, k,   p );   %}
  im    = inline_c %{ return qrtree.geti(    &qrtree, k,   m );   %}
  im1   = inline_c %{ return qrtree.geti(    &qrtree, k+1, m );   %}

  SIMCOST inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? 12 : 6; %}

  : A(m, n)

  RW   A1 <- (   prevp == descA.mt ) ? C  zgessm( k, ip, n ) : A1 zttmqr(k, prevp, n )
          -> (   nextp != descA.mt ) ? A1 zttmqr( k, nextp, n)
          -> ( ( nextp == descA.mt ) & ( p == k ) ) ? A  zttmqr_out_A1(p, n)
          -> ( ( nextp == descA.mt ) & ( p != k ) ) ? A2 zttmqr( k, p, n )

  RW   A2 <- ( (type == 0) && (k     == 0        ) ) ? A(m, n)
          <- ( (type == 0) && (k     != 0        ) ) ? A2 zttmqr(k-1, m, n )
          <- ( (type != 0) && (prevm == descA.mt ) ) ? C zgessm(k, im, n)
          <- ( (type != 0) && (prevm != descA.mt ) ) ? A1 zttmqr(k, prevm, n )
          -> ( (type1 != 0 ) && (n==(k+1)) ) ? A  zgetrf( k+1, im1 )
          -> ( (type1 != 0 ) && (n>  k+1)  ) ? C  zgessm( k+1, im1, n )
          -> ( (type1 == 0 ) && (n==(k+1)) ) ? A2 zttqrt( k+1, m )
          -> ( (type1 == 0 ) && (n> (k+1)) ) ? A2 zttmqr( k+1, m, n )

  READ  V <- (type == 0) ? A2 zttqrt(k, m)
          <- (type != 0) ? A2 zttqrt(k, m) [type = UPPER_TILE]

  READ  L <- L zttqrt(k, m)                                                               [type = SMALL_L]
  READ  P <- (type == 0 ) ? P zttqrt(k, m)                                                [type = PIVOT]
          <- (type != 0 ) ? IPIV(m,n)                                                     [type = PIVOT]

    ; inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? GETPRIO_UPDTE(p, n, k) : GETPRIO_UPDTE(m, n, k); %}

BODY
{
    int tempnn = ((n)==((descA.nt)-1)) ? ((descA.n)-(n*(descA.nb))) : (descA.nb);
    int tempmm = ((m)==((descA.mt)-1)) ? ((descA.m)-(m*(descA.mb))) : (descA.mb);
    int ldap = BLKLDD( descA, p );
    int ldam = BLKLDD( descA, m );

    printlog("zttmqr( k=%d, m=%d, n=%d, p=%d, nextp=%d, prevp=%d, prevm=%d, type=%d, type1=%d, ip=%d, im=%d, im1=%d)\n"
             "\t(descA.mb=%d, tempnn=%d, tempmm=%d, tempnn=%d, ib=%d, \n"
             "\t A(%d,%d)[%p], ldap=%d, A(%d,%d)[%p], ldam = %d,\n"
             "\t LT(%d,%d)[%p], descLT.mb=%d, A(%d,%d)[%p], ldam = %d)\n",
             k, m, n, p, nextp, prevp, prevm, type, type1, ip, im, im1,
             descA.mb, tempnn, tempmm, tempnn, ib,
             p, n, A1, ldap, m, n, A2, ldam,
             m, k, L, descLT.mb, m, k, V, ldam);

#if !defined(DAGUE_DRY_RUN)

        if ( type == DPLASMA_QR_KILLED_BY_TS ) {
            CORE_zssssm(
                descA.mb, tempnn, tempmm, tempnn, descA.nb, ib,
                A1 /* A(p, n)  */, ldap,
                A2 /* A(m, n)  */, ldam,
                L  /* LT(m, k) */, descLT.mb,
                V  /* A(m, k)  */, ldam,
                P  /* IPIV(m,k)*/ );
        } else {
            void *p_elem_A = dague_private_memory_pop( p_work );

            CORE_zttmqr(
                PlasmaLeft, PlasmaConjTrans,
                descA.mb, tempnn, tempmm, tempnn, descA.nb, ib,
                A1 /* A(p, n)  */, ldap,
                A2 /* A(m, n)  */, ldam,
                V  /* A(m, k)  */, ldam,
                L  /* LT(m, k) */, descLT.mb,
                p_elem_A, ib );

            dague_private_memory_push( p_work, p_elem_A );
        }

#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zttmqr_out_A1(k, n) [profile = off]
  k = 0   .. minMN-2
  n = k+1 .. descA.nt-1
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, k, k ); %}

  : A(k, n)

  RW A <- A1 zttmqr( k, prevp, n )
       -> A(k, n)
BODY
{
    /* nothing */
}
END
