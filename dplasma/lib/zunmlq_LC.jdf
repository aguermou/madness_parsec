extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 * $COPYRIGHT
 *
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

side      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
dataB     [type = "dague_ddesc_t *"]
descB     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataB)"]
dataT     [type = "dague_ddesc_t *"]
descT     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataT)"]
ib        [type = "int" hidden = on default = "descT.mb" ]
pool_0    [type = "dague_memory_pool_t *" size = "((sizeof(dague_complex64_t))*ib)*descT.nb"]


zunmlq(k, n)
  /* Execution Space */
  k = 0 .. descA.mt-1
  n = 0 .. descB.nt-1

  /* Locality */
  : dataB( k, n )

  READ  A    <- A zunmlq_in_data_A0(k)   [type = UPPER_TILE]
  READ  T    <- T zunmlq_in_data_T1(k)   [type = LITTLE_T]
  RW    C    -> ( k == 0 ) ? dataB(k, n)
             -> ( k >  0 ) ? A2 ztsmlq(k-1, k, n)
             <- ( k <  (descB.mt-1)) ? A1 ztsmlq(k, k+1, n)
             <- ( k == (descB.mt-1)) ? dataB(k, n)

BODY
{
    int tempkm   = (k == (descB.mt-1)) ? (descB.m - k * descB.mb) : descB.mb;
    int tempnn   = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempkmin = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int ldbk = BLKLDD( descB, k );

    printlog("CORE_zunmlq(%d, %d)\n"
             "\t(side, trans, tempkm, tempnn, tempkmin, ib, A(%d,%d)[%p], ldak, T(%d,%d)[%p], descT.mb, B(%d,%d)[%p], ldbk, p_elem_A, descT.nb)\n",
             k, n, k, k, A, k, k, T, k, n, C);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( pool_0 );

    CORE_zunmlq(side, trans,
                tempkm, tempnn, tempkmin, ib,
                A /* dataA(k,k) */, ldak,
                T /* dataT(k,k) */, descT.mb,
                C /* dataB(k,n) */, ldbk,
                p_elem_A, descT.nb );

    dague_private_memory_push( pool_0, p_elem_A );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

zunmlq_in_data_T1(k) [profile = off]
  /* Execution Space */
  k = 0 .. descA.mt-1

  /* Locality */
  : dataT(k, k)

  READ  T    <- dataT(k, k)                  [type = LITTLE_T]
             -> T zunmlq(k, 0 .. descB.nt-1) [type = LITTLE_T]

BODY
{
    /* nothing */
}
END

zunmlq_in_data_A0(k) [profile = off]
  /* Execution Space */
  k = 0 .. descA.mt-1

  /* Locality */
  : dataA(k, k)

  READ  A    <- dataA(k, k)                   [type = UPPER_TILE]
             -> A zunmlq(k, 0 .. descB.nt-1 ) [type = UPPER_TILE]

BODY
{
    /* nothing */
}
END

ztsmlq(k, m, n)
  /* Execution Space */
  k = 0     .. inline_c %{ return dplasma_imin((descA.nt-2),(descA.mt-1)); %}
  m = (k+1) .. (descB.mt-1)
  n = 0     .. (descB.nt-1)

  /* Locality */
  : dataB(m,n)

  RW    A1   -> ( m == (k+1) ) ? C  zunmlq(k, n)
             -> ( m >  (k+1) ) ? A1 ztsmlq(k, m-1, n)
             <- ( m <  (descB.mt-1) ) ? A1 ztsmlq(k, m+1, n)
             <- ( m == (descB.mt-1) ) ? A1 ztsmlq_out_data_B0(k, n)

  RW    A2   -> ( k == 0 ) ? dataB(m,n)
             -> ( k >  0 ) ? A2 ztsmlq(k-1, m, n)
             <-  (k == (descA.mt-1)) ? dataB(m,n)
             <- ((k <  (descA.mt-1)) & (m == (k+1))) ? C  zunmlq(k+1, n)
             <- ((k <  (descA.mt-1)) & (m >  (k+1))) ? A2 ztsmlq(k+1, m, n)

  READ  V    <- V ztsmlq_in_data_A1(k, m)
  READ  T    <- T ztsmlq_in_data_T2(k, m)  [type = LITTLE_T]

BODY
{
    int tempnn   = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempmm   = (m == (descB.mt-1)) ? (descB.m - m * descB.mb) : descB.mb;
    int tempkmin = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldbk = BLKLDD( descB, k );
    int ldbm = BLKLDD( descB, m );
    int ldak = BLKLDD( descA, k );
    int ldwork = ib;

    printlog("CORE_ztsmlq(%d, %d, %d)\n"
             "\t(side, trans, descB.mb, tempnn, tempmm, tempnn, tempkmin, ib, B(%d,%d)[%p], ldbk, B(%d,%d)[%p], ldbm, A(%d,%d)[%p], ldam, T(%d,%d)[%p], descT.mb, p_elem_A, ldwork)\n",
             k, m, n, k, n, A1, m, n, A2, m, k, V, m, k, T);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( pool_0 );

    CORE_ztsmlq(side, trans,
                descB.mb, tempnn, tempmm, tempnn, tempkmin, ib,
                A1 /* dataB(k, n) */, ldbk,
                A2 /* dataB(m, n) */, ldbm,
                V  /* dataA(k, m) */, ldak,
                T  /* dataT(k, m) */, descT.mb,
                p_elem_A, ldwork );

    dague_private_memory_push( pool_0, p_elem_A );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

ztsmlq_in_data_T2(k, m) [profile = off]
  /* Execution Space */
  k = 0     .. inline_c %{ return dplasma_imin((descB.mt-2),(descA.mt-1)); %}
  m = (k+1) .. (descB.mt-1)

  /* Locality */
  : dataT(k, m)

  READ  T    <- dataT(k, m)                         [type = LITTLE_T]
             -> T ztsmlq(k, m, 0 .. (descB.nt-1))   [type = LITTLE_T]

BODY
{
    /* nothing */
}
END

ztsmlq_in_data_A1(k, m) [profile = off]
  /* Execution Space */
  k = 0     .. inline_c %{ return dplasma_imin((descB.mt-2),(descA.mt-1)); %}
  m = (k+1) .. (descB.mt-1)

  /* Locality */
  : dataA(k, m)

  READ  V    <- dataA(k, m)
             -> V ztsmlq(k, m, 0 .. (descB.nt-1))

BODY
{
    /* nothing */
}
END

ztsmlq_out_data_B0(k, n) [profile = off]
  /* Execution Space */
  k = 0 .. inline_c %{ return dplasma_imin((descB.mt-2),(descA.mt-1)); %}
  n = 0 .. (descB.nt-1)

  /* Locality */
  : dataB(k, n)

  READ  A1   -> A1 ztsmlq(k, descB.mt-1, n)
             <- dataB(k,n)

BODY
{
    /* nothing */
}
END
