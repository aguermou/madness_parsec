extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> z c d s
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

uplo      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
alpha     [type = "dague_complex64_t"]
dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
beta      [type = "dague_complex64_t"]
dataC     [type = "dague_ddesc_t *"]
descC     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataC)"]


zsyrk(n, k)
  /* Execution Space */
  n = 0..(descC.nt-1)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataC(n,n)

  READ  A    <- A zsyrk_in_data_A0(n, k)
  RW    C    <- (0 == k) ? dataC(n,n)
             <- (k >= 1) ? C zsyrk(n, k-1)
             -> ((descA.mt-1) >  k) ? C zsyrk(n, k+1)
             -> ((descA.mt-1) == k) ? dataC(n,n)

BODY
{
    int tempnn = (n==(descC.nt-1)) ? (descC.n-(n*descC.nb)) : descC.nb;
    int tempkm = (k==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    int ldak = BLKLDD( descA, k );
    dague_complex64_t zbeta = (k==0) ? beta : (dague_complex64_t)1.;
    int ldcn = BLKLDD( descC, n );

    printlog("CORE_zsyrk(%d, %d)\n"
             "\t(uplo, trans, tempnn, tempkm, alpha, A(%d,%d)[%p], ldak, zbeta, C(%d,%d)[%p], ldcn)\n",
             n, k, k, n, A, n, n, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zsyrk(uplo, trans, tempnn, tempkm,
               alpha, A /* dataA(k,n) */, ldak,
               zbeta, C /* dataC(n,n) */, ldcn );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zsyrk_in_data_A0(n, k) [profile = off]
  /* Execution Space */
  n = 0..(descC.nt-1)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,n)

  READ  A    <- dataA(k,n)
             -> A zsyrk(n, k)

BODY
{
    /* nothing */
}
END

zgemm(n, m, k)
  /* Execution Space */
  n = 0     .. (descC.mt-2)
  m = (n+1) .. (descC.mt-1)
  k = 0     .. (descA.mt-1)

  /* Locality */
  : dataC(n,m)

  READ  A    <- A zgemm_in_data_A0(n, k)
  READ  B    <- B zgemm_in_data_A1(m, k)
  RW    C    <- ((k>=1)) ? C zgemm(n, m, k-1)
             <- ((0==k)) ? dataC(n,m)
             -> ((descA.mt==(k+1))) ? dataC(n,m)
             -> ((descA.mt>=(2+k))) ? C zgemm(n, m, k+1)

BODY
{
    int tempnn = (n==(descC.nt-1)) ? (descC.n-(n*descC.nb)) : descC.nb;
    int tempmm = ((m)==(descC.mt-1)) ? (descC.m-(m*descC.mb)) : descC.mb;
    int tempkm = (k==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    int ldak = BLKLDD( descA, k );
    dague_complex64_t zbeta = (k==0) ? beta : (dague_complex64_t)1.;
    int ldcn = BLKLDD( descC, n );

    printlog("CORE_zgemm(%d, %d, %d)\n"
             "\t(trans, PlasmaNoTrans, tempnn, tempmm, tempkm, zalpha, A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldak, zbeta, C(%d,%d)[%p], ldcn)\n",
             n, m, k, k, n, A, k, m, B, n, m, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(trans, PlasmaNoTrans, tempnn, tempmm, tempkm,
               alpha,  A /* dataA(k,n) */, ldak,
                       B /* dataA(k,m) */, ldak,
               zbeta,  C /* dataC(n,m) */, ldcn );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zgemm_in_data_A1(m, k) [profile = off]
  /* Execution Space */
  m = 1..(descC.mt-1) /* tight bound is (n+1)..(descC.mt-1) */
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,m)

  READ  B    <- dataA(k,m)
             -> B zgemm(0..(descC.mt-2), m, k)

BODY
{
    /* nothing */
}
END

zgemm_in_data_A0(n, k) [profile = off]
  /* Execution Space */
  n = 0..(descC.mt-2)
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,n)

  READ  A    <- dataA(k,n)
             -> A zgemm(n, (n+1)..(descC.mt-1), k)

BODY
{
    /* nothing */
}
END
