extern "C" %{
#include <sys/time.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include "data_dist/matrix/two_dim_rectangle_cyclic.h"

/**
 * This test stress the startup mechanism by generating NI*NJ*NK independent
 * tasks from the begining. A sequential startup will incur a high overhead,
 * while a more parallel startup will mitigate this overhead.
 *
 * The behavior of the test is altered by the defined priority, -1 and 1 is
 * for an increasing priority, respectively decreasing, 0 is for no priority
 * and 2 is for a random behavior. The generated priory is global, but it does
 * not impose a strict scheduling.
 */
%}

A          [type = "two_dim_block_cyclic_t*"]
NI         [type = int]
NJ         [type = int]
NK         [type = int]
pri        [type = int default = 0 hidden = on]

STARTUP(i, j, k)

  i = 0 .. NI-1
  j = 0 .. NJ-1
  k = 0 .. NK-1

  valid1 = i == 1 && j == 1
  valid2 = (i == 1) && (j == 1)

  prio = inline_c %{ return pri == 2 ? (int)(random()) : ((NJ*NK*i + NK*j + k)*pri); %}

  : A(i,0)

  READ A <- A(i, 0)
         -> A(i, 0)

    ; prio

BODY
{
#if defined(DEBUG_STARTUP_TASK)
    fprintf(stderr, "priority(%d, %d, %d): %d\n",
            i, j, k, prio );
#endif
    assert(valid1 == valid2);
}
END

extern "C" %{

#define NN    10
#define TYPE  matrix_RealFloat

struct my_priority_s {
    int prio;
    char* message;
} priorities[] = {
    { .prio = -1, .message = "Decreasing priority" },
    { .prio =  0, .message = "No priority" },
    { .prio =  1, .message = "Increasing priority" },
    { .prio =  2, .message = "Random priority" },
    { .prio =  0, .message = NULL}};
    
#define TIMER_START(TIMER)                      \
    do {                                        \
        struct timeval tv;                      \
        gettimeofday(&tv,NULL);                 \
        (TIMER) = tv.tv_sec * 1e6 + tv.tv_usec; \
    } while (0)

#define TIMER_STOP(TIMER)                                   \
    do {                                                    \
        struct timeval tv;                                  \
        gettimeofday(&tv,NULL);                             \
        (TIMER) = (tv.tv_sec * 1e6 + tv.tv_usec) - (TIMER); \
    } while (0)

int main( int argc, char** argv )
{
    dague_startup_handle_t* handle;
    two_dim_block_cyclic_t descA;
    dague_arena_t arena;
    dague_context_t *dague;
    int ni = NN, nj = NN, nk = NN, verbose = 0, i = 1;
    long time_elapsed;

#ifdef DAGUE_HAVE_MPI
    {
        int provided;
        MPI_Init_thread(NULL, NULL, MPI_THREAD_SERIALIZED, &provided);
    }
#endif

    while( NULL != argv[i] ) {
        if( 0 == strncmp(argv[i], "-i=", 3) ) {
            ni = strtol(argv[i]+3, NULL, 10);
            goto move_and_continue;
        }
        if( 0 == strncmp(argv[i], "-j=", 3) ) {
            nj = strtol(argv[i]+3, NULL, 10);
            goto move_and_continue;
        }
        if( 0 == strncmp(argv[i], "-k=", 3) ) {
            nk = strtol(argv[i]+3, NULL, 10);
            goto move_and_continue;
        }
        if( 0 == strncmp(argv[i], "-v=", 3) ) {
            verbose = strtol(argv[i]+3, NULL, 10);
            goto move_and_continue;
        }
        i++;  /* skip this one */
        continue;
    move_and_continue:
        memmove(&argv[i], &argv[i+1], (argc - 1) * sizeof(char*));
        argc -= 1;
    }

    dague = dague_init(-1, &argc, &argv);
    if( NULL == dague ) {
       exit(-1);
    }

    /**
     * Build the data and the arena to hold it up.
     */
    two_dim_block_cyclic_init( &descA, TYPE, matrix_Tile,
                               1 /*nodes*/, 0 /*rank*/,
                               NN, NN, ni * NN, NN,
                               0, 0, ni * NN, NN, 1, 1, 1);
    descA.mat = dague_data_allocate( descA.super.nb_local_tiles *
                                     descA.super.bsiz *
                                     dague_datadist_getsizeoftype(TYPE) );
    dague_arena_construct( &arena,
                           descA.super.mb * descA.super.nb * dague_datadist_getsizeoftype(TYPE),
                           DAGUE_ARENA_ALIGNMENT_SSE,
                           DAGUE_DATATYPE_NULL);  /* change for distributed cases */

    srandom((int)getpid());  /* Start the random generator */

    /* Heat up the engine: small tasks no priority */
    handle = dague_startup_new( &descA, ni, nj, nk );
    assert( NULL != handle );
    handle->arenas[DAGUE_startup_DEFAULT_ARENA] = &arena;
    handle->pri = 0;
    dague_enqueue( dague, (dague_handle_t*)handle );
    dague_context_wait(dague);

    for(i = 0; NULL != priorities[i].message; i++) {

        handle = dague_startup_new( &descA, ni, nj, nk );
        assert( NULL != handle );
        TIMER_START(time_elapsed);
        handle->arenas[DAGUE_startup_DEFAULT_ARENA] = &arena;
        handle->pri = priorities[i].prio;
        dague_enqueue( dague, (dague_handle_t*)handle );
        TIMER_STOP(time_elapsed);
        printf("DAG construction [%s] in %ld micro-sec\n",
               priorities[i].message, time_elapsed);
        if( verbose >= 5 ) {
            printf("<DartMeasurement name=\"%s\" type=\"numeric/double\"\n"
                   "                 encoding=\"none\" compression=\"none\">\n"
                   "%g\n"
                   "</DartMeasurement>\n",
                   priorities[i].message, (double)time_elapsed);
        }
        TIMER_START(time_elapsed);
        dague_context_wait(dague);
        TIMER_STOP(time_elapsed);
        printf("DAG execution [%s] in %ld micro-sec\n",
               priorities[i].message, time_elapsed);
    }

    free(descA.mat);

    dague_fini( &dague);

#ifdef DAGUE_HAVE_MPI
    MPI_Finalize();
#endif

    return 0;
}

%}
