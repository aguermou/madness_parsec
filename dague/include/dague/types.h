/*
 * Copyright (c) 2012-2015 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 */


#ifndef DAGUE_TYPES_H_HAS_BEEN_INCLUDED
#define DAGUE_TYPES_H_HAS_BEEN_INCLUDED

typedef struct dague_data_s dague_data_t;
typedef struct dague_data_copy_s dague_data_copy_t;
typedef uint32_t dague_data_key_t;
typedef struct dague_ddesc_s dague_ddesc_t;

#endif  /* DAGUE_TYPES_H_HAS_BEEN_INCLUDED */

