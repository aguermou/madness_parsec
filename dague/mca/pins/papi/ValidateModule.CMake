# Always look for PAPI to avoid CMake complaints about manually-specified
# variables not used.
find_package(PAPI QUIET)

if (PINS_ENABLE)
  if (NOT DAGUE_PROF_TRACE)
    MESSAGE(STATUS "Module ${MODULE} not selectable: DAGUE_PROF_TRACE disabled.")
  else (NOT DAGUE_PROF_TRACE)
    set(DAGUE_HAVE_PAPI ${PAPI_FOUND})
    if (NOT PAPI_FOUND)
      set(DAGUE_HAVE_PAPI ${PAPI_FOUND} PARENT_SCOPE)
      set(PAPI_FOUND ${PAPI_FOUND} PARENT_SCOPE)
      if (PAPI_FOUND)
	list(APPEND EXTRA_LIBS ${PAPI_LIBRARY})
	include_directories( ${PAPI_INCLUDE_DIR} )
      endif (PAPI_FOUND)
    endif (NOT PAPI_FOUND)

    if (DAGUE_HAVE_PAPI)
      SET(MCA_${COMPONENT}_${MODULE} ON)
      FILE(GLOB MCA_${COMPONENT}_${MODULE}_SOURCES mca/pins/pins_papi_utils.c ${MCA_BASE_DIR}/${COMPONENT}/${MODULE}/[^\\.]*.c)
      SET(MCA_${COMPONENT}_${MODULE}_CONSTRUCTOR "${COMPONENT}_${MODULE}_static_component")
      include_directories( ${PAPI_INCLUDE_DIR} )
      list(APPEND EXTRA_LIBS ${PAPI_LIBRARY})
    else (DAGUE_HAVE_PAPI)
      MESSAGE(STATUS "Module ${MODULE} not selectable: does not have PAPI")
      SET(MCA_${COMPONENT}_${MODULE} OFF)
    endif( DAGUE_HAVE_PAPI )
  endif (NOT DAGUE_PROF_TRACE)
else (PINS_ENABLE)
  MESSAGE(STATUS "Module ${MODULE} not selectable: PINS disabled.")
  SET(MCA_${COMPONENT}_${MODULE} OFF)
endif (PINS_ENABLE)
