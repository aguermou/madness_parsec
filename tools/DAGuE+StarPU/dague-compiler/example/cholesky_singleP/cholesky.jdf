extern "C" %{
#include <plasma.h>

#include "magma.h"
#include <core_blas.h>
#include "cublas.h"
#include "time.h"
#include "cuda.h"
#include <starpu.h>
#include "dague.h"
#include "cholesky_data.h"

#include "precision.h"

#include "magma_s.h"


%}

NB           [type = int]
SIZE         [type = int]
uplo         [type = PLASMA_enum]
INFO         [type = "int*"]

/**************************************************
 *                      POTRF                     *
 **************************************************/
POTRF(k) [high_priority = on]

// Execution space
k = 0..SIZE-1

// Parallel partitioning
: A(k, k)

// Parameters
RW T <- (k == 0) ? A(k, k) : T HERK(k-1, k)
     -> T TRSM(k+1..SIZE-1, k)
     -> A(k, k)

// For the CPU
BODY
    CORE_spotrf(uplo, NB, T, NB, INFO);
END


BODY CUDA
    if(uplo == PlasmaLower)
    	    magma_spotrf_gpu('L', NB, (float*) T, NB, INFO);
    else
            magma_spotrf_gpu('U', NB, (float*) T, NB, INFO);

     cudaDeviceSynchronize();
END


/**************************************************
 *                      TRSM                      *
 **************************************************/
TRSM(m, k) [high_priority = on]
// Execution space
m = 1..SIZE-1
k = 0..m-1

// Parallel partitioning
: A(m, k)

// Parameters
READ  T <- T POTRF(k)
RW    C <- (k == 0) ? A(m, k) : C GEMM(k-1, m, k)
        -> A HERK(k, m)
        -> A GEMM(k, m, k+1..m-1)
        -> B GEMM(k, m+1..SIZE-1, m)
        -> A(m, k)

// For the CPU
BODY
    if( uplo == PlasmaLower ) {
       CORE_strsm(PlasmaRight, PlasmaLower, PlasmaTrans, PlasmaNonUnit,
                   NB, NB, (float)1.0, T, NB, C, NB);
    } else { 
       CORE_strsm(PlasmaLeft, PlasmaUpper, PlasmaTrans, PlasmaNonUnit,
                   NB, NB, (float)1.0, T, NB, C, NB );
    }
END


// For the CUDA
BODY CUDA
    if( uplo == PlasmaLower ) {
    	cublasStrsm((char)PlasmaRight, (char)PlasmaLower,
	            (char)PlasmaTrans, (char)PlasmaNonUnit, NB, NB,
    	 	    (float) 1.0,
		    (float *) (T), NB, 
		    (float *) (C), NB); 

    } else {
      	cublasStrsm((char)PlasmaLeft, (char)PlasmaUpper,
		    (char)PlasmaTrans, (char)PlasmaNonUnit, NB, NB,
		    (float) 1.0,
		    (float *) (T), NB, 
		    (float *) (C), NB); 
   
    }
    cudaDeviceSynchronize();
END


/**************************************************
 *                      HERK                      *
 **************************************************/
HERK(k, n) [high_priority = on]
// Execution space
k = 0..SIZE-2
n = k+1..SIZE-1

// Parallel partitioning
: A(n, n)

//Parameters
READ  A <- C TRSM(n, k)
RW    T <- (k == 0)   ? A(n,n)     : T HERK(k-1, n)
        -> (n == k+1) ? T POTRF(n) : T HERK(k+1, n)

// For the CPU
BODY
    if( uplo == PlasmaLower ) {
        CORE_ssyrk(PlasmaLower, PlasmaNoTrans, NB, NB,
                   (float)-1.0, A, NB, (float) 1.0, T, NB );
    } else {
        CORE_ssyrk(PlasmaUpper, PlasmaTrans, NB, NB,
                  (float)-1.0, A, NB, (float) 1.0, T, NB);
    }
END


// For the CUDA
BODY CUDA
    if( uplo == PlasmaLower ) {
        cublasSsyrk(PlasmaLower, PlasmaNoTrans, NB, NB,		
	            (float)-1.0, 
		    (float *) (A), NB, 
	  	    (float) 1.0, 
       		    (float *) (T), NB);
    } else {
       	cublasSsyrk(PlasmaUpper, PlasmaTrans, NB, NB,		
	            (float)-1.0, 
		    (float *) (A), NB, 
	  	    (float) 1.0, 
       		    (float *) (T), NB);
    }
    cudaDeviceSynchronize();
END


/**************************************************
 *                      GEMM                      *
 **************************************************/
GEMM(k, m, n)
// Execution space
k = 0..SIZE-3
m = k+2..SIZE-1
n = k+1..m-1

// Parallel partitioning
: A(m, n)

// Parameters
READ  A <- C TRSM(m, k)
READ  B <- C TRSM(n, k)
RW    C <- (k == 0)   ? A(m, n)     : C GEMM(k-1, m, n)
        -> (n == k+1) ? C TRSM(m,n) : C GEMM(k+1, m, n)

// For the CPU
BODY
    if( uplo == PlasmaLower ) {
        CORE_sgemm(PlasmaNoTrans, PlasmaTrans, NB, NB, NB,
                   (float)-1.0, A, NB, B, NB,
                   (float) 1.0, C, NB);
    } else {
        CORE_sgemm(PlasmaTrans, PlasmaNoTrans, NB, NB, NB,
                   (float)-1.0, A, NB, B, NB,
                   (float) 1.0, C, NB);
    }

END

//For the CUDA
BODY CUDA
    if( uplo == PlasmaLower ){
        cublasSgemm(PlasmaNoTrans, PlasmaTrans, NB, NB, NB,
    	   	    (float)  -1.0,
		    (float*) (B), NB,
       		    (float*) (A), NB,
    	 	    (float)  1.0,
		    (float*) (C), NB);
    } else {
        cublasSgemm(PlasmaTrans, PlasmaNoTrans, NB, NB, NB,
      	 	    (float)  -1.0,
	  	    (float *) (A), NB,
       		    (float *) (B), NB,
    	 	    (float)   1.0,
		    (float *) (C), NB);
    }
    cudaDeviceSynchronize();
END
